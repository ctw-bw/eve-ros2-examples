#!/usr/bin/env python3

import rclpy
import rclpy.qos
from halodi_msgs.msg import (
    WholeBodyState,
    JointName,
    JointSpaceCommand,
    WholeBodyControllerCommand
)
from rclpy.node import Node
from time import sleep


def generate_joint_space_acc_command_msg(joint_id, qdd_desired):

    msg_ = JointSpaceCommand(joint=JointName(joint_id=joint_id))

    # Disable PD control such that only feedforward acc control is used, setting the acceleration command to zero then sets low impedance joints
    msg_.use_default_gains = False
    msg_.stiffness = 0.0
    msg_.damping = 0.0
    # Not sure what dampingscale does
    msg_.motorDampingScale = 0.0
    msg_.qdd_desired = qdd_desired

    return msg_

def generate_joint_space_pos_command_msg(joint_id, q_desired):

    msg_ = JointSpaceCommand(joint=JointName(joint_id=joint_id))

    # Disable PD control such that only feedforward acc control is used, setting the acceleration command to zero then sets low impedance joints
    msg_.use_default_gains = True
    msg_.stiffness = 0.0
    msg_.damping = 0.0
    # Not sure what dampingscale does
    msg_.motorDampingScale = 0.0
    msg_.q_desired = q_desired

    return msg_

class WholeBodyCommandPublisher(Node):

    def __init__(self, whole_body_command_msg=None):
        super().__init__(
            "low_imp_rt"
        )  # initialize the underlying Node with the name low_impedance_rt

        # 10 is overloaded for being 10 deep history QoS
        self._publisher = self.create_publisher(
            WholeBodyControllerCommand, "/eve/whole_body_command", rclpy.qos.qos_profile_system_default
        )

        # 10 is overloaded for being 10 deep history QoS
        self._subscriber = self.create_subscription(
            WholeBodyState, "/eve/whole_body_state", self.whole_body_state_cb, rclpy.qos.qos_profile_sensor_data
        )

        # Attach the message defined in main to self
        self._whole_body_command_msg = whole_body_command_msg

    def run(self):
        # Send the message
        self._publisher.publish(self._whole_body_command_msg)

    def whole_body_state_cb(self, msg):

        # Initialize 
        whole_body_command_msg_ = WholeBodyControllerCommand()

        # Make left and/or right high impedance
        left = True 
        right = True

        # The joint are just numbers
        joint_act = 12
        joint_sen = 10
        # for joint in left*list(range(JointName.LEFT_SHOULDER_PITCH, JointName.LEFT_WRIST_ROLL + 1)) + right*list(range(JointName.RIGHT_SHOULDER_PITCH, JointName.RIGHT_WRIST_ROLL + 1)):
            # Get the position of the current joint
        pos = msg.joint_states[joint_sen].position
        # print(joint, pos)
        # Append the current pos PD command for each joint in the loop
        whole_body_command_msg_.joint_space_commands.append(generate_joint_space_pos_command_msg(joint_act, pos))
        
        
        # self._publisher.publish(whole_body_command_msg_)

        # self.
        # self.destroy_node()



def main():

    # Initialize rclpy
    rclpy.init()  

    # Initialize the node, sleep to allow setup of the ros node or rclpy (takes some asynchornous time), then send the message once
    node = WholeBodyCommandPublisher()

    # Spin here keeps the script waiting for what its subscribed to instead of finishing
    # try:
    rclpy.spin(node)
    # except KeyboardInterrupt:
    #     pass
        
    rclpy.shutdown()


if __name__ == "__main__":
    main()
