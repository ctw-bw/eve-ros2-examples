#!/usr/bin/env python3

import uuid

import numpy as np
import rclpy
import rclpy.qos
from action_msgs.msg import GoalStatus
from builtin_interfaces.msg import Duration
from std_msgs.msg import String
from halodi_msgs.msg import (
    JointName,
    JointSpaceCommand,
    ReferenceFrameName,
    TaskSpaceCommand,
    TrajectoryInterpolation,
    WholeBodyTrajectory,
    WholeBodyTrajectoryPoint,
    DrivingCommand,
)
from rclpy.node import Node
from scipy.spatial.transform import Rotation
from unique_identifier_msgs.msg import UUID

def generate_joint_space_command_msg(
    joint_id, q_desired, qd_desired=0.0, qdd_desired=0.0
):

    msg_ = JointSpaceCommand(joint=JointName(joint_id=joint_id), use_default_gains=True)
    msg_.q_desired = q_desired
    msg_.qd_desired = qd_desired
    msg_.qdd_desired = qdd_desired

    return msg_

def generate_task_space_command_msg(
    body_frame_id, expressed_in_frame_id, xyzrpy, z_up=True
):

    msg_ = TaskSpaceCommand(express_in_z_up=z_up)
    msg_.body_frame.frame_id = body_frame_id
    msg_.expressed_in_frame.frame_id = expressed_in_frame_id

    msg_.pose.position.x = xyzrpy[0]
    msg_.pose.position.y = xyzrpy[1]
    msg_.pose.position.z = xyzrpy[2]
    quat_ = Rotation.from_euler("xyz", xyzrpy[3:]).as_quat()  # Euler to quaternion
    msg_.pose.orientation.x = quat_[0]
    msg_.pose.orientation.y = quat_[1]
    msg_.pose.orientation.z = quat_[2]
    msg_.pose.orientation.w = quat_[3]

    return msg_

def generate_drive_msg(
    lin_vel, rot_vel
):

    msg = DrivingCommand()
    msg.linear_velocity = lin_vel
    msg.angular_velocity = rot_vel

    return msg

class DemoRibbonFull(Node):
    def __init__(self) :
        super().__init__("demo_ribbon_full")

        self.publisher_whole_body_trajectory = self.create_publisher(
            WholeBodyTrajectory, "/eve/whole_body_trajectory", rclpy.qos.qos_profile_action_status_default 
        )

        self.publisher_driving_command = self.create_publisher(
            DrivingCommand, "/eve/driving_command", rclpy.qos.qos_profile_action_status_default 
        )

        self.t = 0.0
        self.dt = 0.01

        self.t_look_down_half = 2
        self.t_drive_up = 2
        self.t_delay_drive_up = 2
        self.t_look_down = 2
        self.t_scissors = 4
        self.t_scissors_wait = 2
        self.t_look_up = 1
        self.t_delay_look = 0.5
        self.t_look_wait = 0.5
        self.t_arms_up = 1
        self.t_drive_through = 5
        self.t_arms_down = 4
        self.t_delay_drive_through = 2
        self.t_breathe = 1000

        self.linear_velocity_up = 0.2
        self.linear_velocity_through = 0.5

        self.index_previous = -1

        # functions to call, their respective times and cumulative times
        self.functions =    [self.look_down_half,   self.drive_up,      self.delay,                self.look_down,     self.scissors,      self.scissors_wait,     self.look_up,       self.delay,         self.look_wait,     self.arms_up,       self.drive_through,     self.delay,                 self.arms_down,    self.breathe]
        self.t_intervals =  [self.t_look_down_half, self.t_drive_up,    self.t_delay_drive_up,     self.t_look_down,   self.t_scissors,    self.t_scissors_wait,   self.t_look_up,     self.t_delay_look,  self.t_look_wait,   self.t_arms_up,     self.t_drive_through,   self.t_delay_drive_through, self.t_arms_down,  self.t_breathe]
        
        # self.functions = [self.arms_down]
        # self.t_intervals = [self.t_arms_down]

        # self.functions = [self.breathe]
        # self.t_intervals = [self.t_breathe]

        # self.functions = [self.scissors]
        # self.t_intervals = [self.t_scissors]
        
        self.t_cumulative = np.cumsum(self.t_intervals)

        self.timer = self.create_timer(self.dt, self.timer_cb)
        
    def delay(self, t):

        if self.index != self.index_previous:
            print('delay')
        pass

    def timer_cb(self):
        self.t += self.dt

        # which index of the functions/times currently
        self.index = sum(self.t_cumulative <= self.t)

        # current ones at these indices
        self.current_function = self.functions[self.index]
        self.current_interval = self.t_intervals[self.index]

        # run the current time's function
        self.current_function(self.current_interval)

        # save to know when there has been a change (some commands must be continuous, some only once)
        self.index_previous = self.index

    def look_down_half(self, t):
        # Look to ribbon
        
        if self.index != self.index_previous:
            print('look_down')

            point = WholeBodyTrajectoryPoint(
                time_from_start = Duration(sec = t)
            )
            point.joint_space_commands.append(
                generate_joint_space_command_msg(JointName.NECK_PITCH, 0.3)
            )  

            msg = WholeBodyTrajectory()
            msg.interpolation_mode.value = (
                TrajectoryInterpolation.MINIMUM_JERK_CONSTRAINED
            )  
            
            msg.trajectory_points.append(point)
            self.publisher_whole_body_trajectory.publish(msg)

    def drive_up(self, t):
        # Drive towards ribbon

        if self.index != self.index_previous:
            print('drive_up')

        msg = generate_drive_msg(self.linear_velocity_up, 0.0)

        self.publisher_driving_command.publish(msg)

    def look_down(self, t):
        # Look to ribbon
        
        if self.index != self.index_previous:
            print('look_down')

            point = WholeBodyTrajectoryPoint(
                time_from_start = Duration(sec = t)
            )
            point.joint_space_commands.append(
                generate_joint_space_command_msg(JointName.NECK_PITCH, 0.5)
            )  

            msg = WholeBodyTrajectory()
            msg.interpolation_mode.value = (
                TrajectoryInterpolation.MINIMUM_JERK_CONSTRAINED
            )  
            
            msg.trajectory_points.append(point)
            self.publisher_whole_body_trajectory.publish(msg)

    def scissors(self, t):
        # Move scissors to forward position

        if self.index != self.index_previous:
            print('scissors')
            
            point = WholeBodyTrajectoryPoint(
                time_from_start = Duration(sec = t)
            )
            point.task_space_commands.append(
                generate_task_space_command_msg(
                    ReferenceFrameName.RIGHT_HAND,
                    ReferenceFrameName.PELVIS,
                    [0.35, -0.25, 0.05, 0.0, np.deg2rad(-70.0), np.deg2rad(20.0)],
                )
            )
            # point.task_space_commands.append(
            #     generate_task_space_command_msg(
            #         ReferenceFrameName.LEFT_HAND,
            #         ReferenceFrameName.PELVIS,
            #         [0.35, 0.25, 0.15, 0.0, np.deg2rad(-70.0), np.deg2rad(-20.0)],
            #     )
            # )

            msg = WholeBodyTrajectory()
            msg.interpolation_mode.value = (
                TrajectoryInterpolation.MINIMUM_JERK_CONSTRAINED
            )  
            
            msg.trajectory_points.append(point)
            self.publisher_whole_body_trajectory.publish(msg)

    def scissors_wait(self, t):
        # Dramatic delay

        if self.index != self.index_previous:
            print('scissors_wait')

    def look_up(self, t):
        # Dramatic look up

        if self.index != self.index_previous:
            print('look_up')

            point = WholeBodyTrajectoryPoint(
                time_from_start = Duration(sec = t)
            )
            point.joint_space_commands.append(
                generate_joint_space_command_msg(JointName.NECK_PITCH, -0.0)
            )  

            msg = WholeBodyTrajectory()
            msg.interpolation_mode.value = (
                TrajectoryInterpolation.MINIMUM_JERK_CONSTRAINED
            )  
            
            msg.trajectory_points.append(point)
            self.publisher_whole_body_trajectory.publish(msg)

    def look_wait(self, t):
        # Dramatic wait

        if self.index != self.index_previous:
            print('look_wait')

    def arms_up(self, t):
        # Arms in the air fast

        if self.index != self.index_previous:
            print('arms_up')

            point = WholeBodyTrajectoryPoint(
                time_from_start = Duration(sec = t)
            )
            # point.task_space_commands.append(
            #     generate_task_space_command_msg(
            #         ReferenceFrameName.RIGHT_HAND,
            #         ReferenceFrameName.PELVIS,
            #         [0.35, -0.25, 0.5, 0.0, np.deg2rad(-120.0), np.deg2rad(20.0)],
            #     )
            # )
            # point.task_space_commands.append(
            #     generate_task_space_command_msg(
            #         ReferenceFrameName.LEFT_HAND,
            #         ReferenceFrameName.PELVIS,
            #         [0.35, 0.25, 0.5, 0.0, np.deg2rad(-120.0), np.deg2rad(-20.0)],
            #     )
            # )
            # point.joint_space_commands.append(
            #     generate_joint_space_command_msg(JointName.RIGHT_SHOULDER_PITCH, -2.3)
            # )
            # point.joint_space_commands.append(
            #     generate_joint_space_command_msg(JointName.RIGHT_SHOULDER_ROLL, -0.8)
            # )
            # point.joint_space_commands.append(
            #     generate_joint_space_command_msg(JointName.LEFT_SHOULDER_PITCH, -2.3)
            # )
            # point.joint_space_commands.append(
            #     generate_joint_space_command_msg(JointName.LEFT_SHOULDER_ROLL, 0.8)
            # )

            point.task_space_commands.append(
                generate_task_space_command_msg(
                    ReferenceFrameName.RIGHT_HAND,
                    ReferenceFrameName.PELVIS,
                    [0.25, -0.55, 0.9, np.deg2rad(20.0), -np.deg2rad(180.0), -np.deg2rad(90.0)],
                )
            )  
            point.task_space_commands.append(
                generate_task_space_command_msg(
                    ReferenceFrameName.LEFT_HAND,
                    ReferenceFrameName.PELVIS,
                    [0.25, 0.55, 0.9, -np.deg2rad(20.0), -np.deg2rad(180.0), np.deg2rad(90.0)],
                )
            )  

            msg = WholeBodyTrajectory()
            msg.interpolation_mode.value = (
                TrajectoryInterpolation.MINIMUM_JERK_CONSTRAINED
            )  
            
            msg.trajectory_points.append(point)
            self.publisher_whole_body_trajectory.publish(msg)

    def drive_through(self, t):
        # Drive for a bit

        if self.index != self.index_previous:
            print('drive_through')

        msg = generate_drive_msg(self.linear_velocity_through, 0.0)
        self.publisher_driving_command.publish(msg)

    def arms_down(self, t):
        # Relax arms

        if self.index != self.index_previous:
            print('arms_down')

            point = WholeBodyTrajectoryPoint(
                time_from_start = Duration(sec = t)
            )
            # point.task_space_commands.append(
            #     generate_task_space_command_msg(
            #         ReferenceFrameName.RIGHT_HAND,
            #         ReferenceFrameName.PELVIS,
            #         [0.35, -0.25, 0.15, 0.0, -np.deg2rad(70.0), 0.0],
            #     )
            # )
            # point.task_space_commands.append(
            #     generate_task_space_command_msg(
            #         ReferenceFrameName.LEFT_HAND,
            #         ReferenceFrameName.PELVIS,
            #         [0.35, 0.25, 0.15, 0.0, -np.deg2rad(70.0), 0.0],
            #     )
            # )

            # point.task_space_commands.append(
            #     generate_task_space_command_msg(
            #         ReferenceFrameName.RIGHT_HAND,
            #         ReferenceFrameName.PELVIS,
            #         [0.25, -0.3, 0.15, -0.0, -np.deg2rad(75.0), 0.0],
            #     )
            # )  
            # point.task_space_commands.append(
            #     generate_task_space_command_msg(
            #         ReferenceFrameName.LEFT_HAND,
            #         ReferenceFrameName.PELVIS,
            #         [0.25, 0.3, 0.15, -0.0, -np.deg2rad(75.0), 0.0],
            #     )
            # )  

            point.task_space_commands.append(
                generate_task_space_command_msg(
                    ReferenceFrameName.RIGHT_HAND,
                    ReferenceFrameName.PELVIS,
                    [0.1, -0.3, 0.0, -0.0, -np.deg2rad(45.0), 0.0],
                )
            )  
            point.task_space_commands.append(
                generate_task_space_command_msg(
                    ReferenceFrameName.LEFT_HAND,
                    ReferenceFrameName.PELVIS,
                    [0.1, 0.3, 0.0, -0.0, -np.deg2rad(45.0), 0.0],
                )
            )  

            msg = WholeBodyTrajectory()
            msg.interpolation_mode.value = (
                TrajectoryInterpolation.MINIMUM_JERK_CONSTRAINED
            )  
            
            msg.trajectory_points.append(point)
            self.publisher_whole_body_trajectory.publish(msg)

    def breathe(self, t):
        # Breathe endlessely

        if self.index != self.index_previous:
            print('breathe')

            self.create_timer(6, self.breathe_cb)
            self.breathe_cb()

    def breathe_cb(self):
        cs = 0

        angle = -0.075

        cs = cs + 3
        point_1 = WholeBodyTrajectoryPoint(
            time_from_start=Duration(sec=cs)
        )
        point_1.joint_space_commands.append(
            generate_joint_space_command_msg(JointName.NECK_PITCH, angle)
        )

        cs = cs + 0
        point_2 = WholeBodyTrajectoryPoint(
            time_from_start=Duration(sec=cs)
        )
        point_2.joint_space_commands.append(
            generate_joint_space_command_msg(JointName.NECK_PITCH, angle)
        )

        cs = cs + 3
        point_3 = WholeBodyTrajectoryPoint(
            time_from_start=Duration(sec=cs)
        )
        point_3.joint_space_commands.append(
            generate_joint_space_command_msg(JointName.NECK_PITCH, -0.0)
        )

        cs = cs + 0
        point_4 = WholeBodyTrajectoryPoint(
            time_from_start=Duration(sec=cs)
        )
        point_4.joint_space_commands.append(
            generate_joint_space_command_msg(JointName.NECK_PITCH, -0.0)
        )


        msg = WholeBodyTrajectory()
        msg.interpolation_mode.value = (
            TrajectoryInterpolation.MINIMUM_JERK_CONSTRAINED
        )  
        
        msg.trajectory_points.append(point_1)
        # msg.trajectory_points.append(point_2)
        msg.trajectory_points.append(point_3)
        # msg.trajectory_points.append(point_4)       

        self.publisher_whole_body_trajectory.publish(msg)     

def main():

    rclpy.init()  

    node = DemoRibbonFull()

    # Spin here keeps the script waiting for what its subscribed to instead of finishing
    try:
        rclpy.spin(node)
    # until ctrl+c is pressed, then exit the interrupts and finish the main()
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()

if __name__ == "__main__":
    main()