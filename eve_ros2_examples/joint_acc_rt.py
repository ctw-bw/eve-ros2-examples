#!/usr/bin/env python3

import rclpy
import rclpy.qos
from halodi_msgs.msg import (
    JointName,
    JointSpaceCommand,
    WholeBodyControllerCommand,
    WholeBodyState
)
from rclpy.node import Node



def generate_joint_space_acc_command_msg(joint_id, qdd_desired):

    msg_ = JointSpaceCommand(joint=JointName(joint_id=joint_id))
    # Disable PD control such that only feedforward acc control
    msg_.use_default_gains = False
    msg_.stiffness = 0.0
    msg_.damping = 0.0
    msg_.qdd_desired = qdd_desired

    return msg_

class WholeBodyCommandPublisher(Node):
    """A helper/example class to publish whole body controller messages.
    """

    def __init__(self, whole_body_command_msg=None):
        super().__init__(
            "joint_acc_rt"
        )

        # Proper implementation subscribes and waits for WholeBodyState and then sends its commands
        self._publisher = self.create_publisher(
            WholeBodyControllerCommand, "/eve/whole_body_command", rclpy.qos.qos_profile_system_default
        )

        # Subscribe to WholeBodyState, which should return at around 500Hz
        self._subscriber = self.create_subscription(
            WholeBodyState, "/eve/whole_body_state", self.whole_body_state_cb, rclpy.qos.qos_profile_sensor_data
        )

        # The joint to set torque to and amount
        self.joint = JointName.NECK_PITCH
        self.acc_ref = -1.0

        # acc                   measure
        #
        # 'HIP_YAW': 0, 
        # 'HIP_ROLL': 1, 
        # 'HIP_PITCH': 2, 
        # 'KNEE_PITCH': 3, 
        # 'ANKLE_ROLL': 4, 
        # 'ANKLE_PITCH': 5, 
        # 'LEFT_SHOULDER_PITCH': 6, LEFT_SHOULDER_YAW      8      AND NOT RIGHT_WRIST_PITCH 
        # 'LEFT_SHOULDER_ROLL': 7, LEFT_ELBOW_PITCH  9               or RIGHT_WRIST_ROLL
        # 'LEFT_SHOULDER_YAW': 8, LEFT_ELBOW_YAW 10
        # 'LEFT_ELBOW_PITCH': 9, LEFT_WRIST_PITCH 11
        # 'LEFT_ELBOW_YAW': 10, LEFT_WRIST_ROLL 12
        # 'LEFT_WRIST_PITCH': 11, RIGHT_SHOULDER_PITCH 13
        # 'LEFT_WRIST_ROLL': 12, RIGHT_SHOULDER_ROLL 14
        # 'RIGHT_SHOULDER_PITCH': 13, RIGHT_ELBOW_PITCH 16             NOT LEFT_SHOULDER_PITCH
        # 'RIGHT_SHOULDER_ROLL': 14, RIGHT_ELBOW_YAW 17               OR LEFT_SHOULDER_ROLL
        # 'RIGHT_SHOULDER_YAW': 15, RIGHT_WRIST_PITCH 18              AND NOT LEFT_SHOULDER_YAW
        # 'RIGHT_ELBOW_PITCH': 16, RIGHT_WRIST_ROLL 19                OR LEFT_ELBOW_PITCH
        # 'RIGHT_ELBOW_YAW': 17, NECK_PITCH 20
        # 'RIGHT_WRIST_PITCH': 18, LEFT_WHEEL 21
        # 'RIGHT_WRIST_ROLL': 19, RIGHT_WHEEL 22
        # 'NECK_PITCH': 20, RIGHT_SHOULDER_YAW 15
        # 'LEFT_WHEEL': 21,
        # 'RIGHT_WHEEL': 22,
        #
        #
        #
        #


    def whole_body_state_cb(self, msg):
        
        # Create message
        self._whole_body_command_msg = WholeBodyControllerCommand();
        self._whole_body_command_msg.joint_space_commands.append(generate_joint_space_acc_command_msg(self.joint, self.acc_ref))
        self.get_logger().info("Set acceleration reference")

        # Send message 
        self._publisher.publish(self._whole_body_command_msg)

    def reset(self):
        # Create message
        self._whole_body_command_msg = WholeBodyControllerCommand();
        self._whole_body_command_msg.joint_space_commands.append(generate_joint_space_acc_command_msg(self.joint, 0.0))
        self.get_logger().info("Set zero acceleration reference, stopped")

        # Send message 
        self._publisher.publish(self._whole_body_command_msg)


def main():

    rclpy.init()  

    node = WholeBodyCommandPublisher()

    # Spin here keeps the program running instead of finishing, such that the ticker can interrupt still, don't know why this doesn't work with a while True loop
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.reset()
        pass
        
    rclpy.shutdown()

if __name__ == "__main__":
    main()
