from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='eve_ros2_examples',
            executable='demo_drive_up.py',
            # These names can replace the original node name
            # namespace='namespace',
            # name='sim'
        ),
        Node(
            package='eve_ros2_examples',
            executable='demo_drive_through.py',
        ),
    ])