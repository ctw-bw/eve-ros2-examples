#!/usr/bin/env python3

import uuid

import numpy as np
import rclpy
import rclpy.qos
from action_msgs.msg import GoalStatus
from builtin_interfaces.msg import Duration
from std_msgs.msg import String
from halodi_msgs.msg import (
    JointName,
    JointSpaceCommand,
    ReferenceFrameName,
    TaskSpaceCommand,
    TrajectoryInterpolation,
    WholeBodyTrajectory,
    WholeBodyTrajectoryPoint,
    DrivingCommand,
)
from rclpy.node import Node
from scipy.spatial.transform import Rotation
from unique_identifier_msgs.msg import UUID

def generate_joint_space_command_msg(
    joint_id, q_desired, qd_desired=0.0, qdd_desired=0.0
):

    msg_ = JointSpaceCommand(joint=JointName(joint_id=joint_id), use_default_gains=True)
    msg_.q_desired = q_desired
    msg_.qd_desired = qd_desired
    msg_.qdd_desired = qdd_desired

    return msg_

def generate_task_space_command_msg(
    body_frame_id, expressed_in_frame_id, xyzrpy, z_up=True
):

    msg_ = TaskSpaceCommand(express_in_z_up=z_up)
    msg_.body_frame.frame_id = body_frame_id
    msg_.expressed_in_frame.frame_id = expressed_in_frame_id

    msg_.pose.position.x = xyzrpy[0]
    msg_.pose.position.y = xyzrpy[1]
    msg_.pose.position.z = xyzrpy[2]
    quat_ = Rotation.from_euler("xyz", xyzrpy[3:]).as_quat()  # Euler to quaternion
    msg_.pose.orientation.x = quat_[0]
    msg_.pose.orientation.y = quat_[1]
    msg_.pose.orientation.z = quat_[2]
    msg_.pose.orientation.w = quat_[3]

    return msg_

def generate_drive_msg(
    lin_vel, rot_vel
):

    msg = DrivingCommand()
    msg.linear_velocity = lin_vel
    msg.angular_velocity = rot_vel

    return msg

class DemoRibbonDefault(Node):
    def __init__(self) :
        super().__init__("demo_ribbon_default")

        self.publisher_whole_body_trajectory = self.create_publisher(
            WholeBodyTrajectory, "/eve/whole_body_trajectory", rclpy.qos.qos_profile_action_status_default 
        )

    def scissors_default(self, t):
            
            point = WholeBodyTrajectoryPoint(
                time_from_start = Duration(sec = t)
            )

            # point.task_space_commands.append(
            #     generate_task_space_command_msg(
            #         ReferenceFrameName.RIGHT_HAND,
            #         ReferenceFrameName.PELVIS,
            #         [0.25, -0.25, 0.25, 0.0, np.deg2rad(-70.0), np.deg2rad(20.0)],
            #     )
            # )
            # point.task_space_commands.append(
            #     generate_task_space_command_msg(
            #         ReferenceFrameName.LEFT_HAND,
            #         ReferenceFrameName.PELVIS,
            #         [0.25, 0.25, 0.25, 0.0, np.deg2rad(-70.0), np.deg2rad(-20.0)],
            #     )
            # )

            point.task_space_commands.append(
                generate_task_space_command_msg(
                    ReferenceFrameName.RIGHT_HAND,
                    ReferenceFrameName.PELVIS,
                    [0.25, -0.3, 0.15, 0.0, -np.deg2rad(75.0), 0.0],
                )
            )  
            point.task_space_commands.append(
                generate_task_space_command_msg(
                    ReferenceFrameName.LEFT_HAND,
                    ReferenceFrameName.PELVIS,
                    [0.1, 0.3, 0.0, -0.0, -np.deg2rad(35.0), 0.0],
                )
            )  

            point.joint_space_commands.append(
                generate_joint_space_command_msg(JointName.NECK_PITCH, 0.0)
            )  

            msg = WholeBodyTrajectory()
            msg.interpolation_mode.value = (
                TrajectoryInterpolation.MINIMUM_JERK_CONSTRAINED
            )  
            
            msg.trajectory_points.append(point)
            self.publisher_whole_body_trajectory.publish(msg)

def main():

    rclpy.init()  

    node = DemoRibbonDefault()

    node.scissors_default(2)

    node.destroy_node()

    rclpy.shutdown()

if __name__ == "__main__":
    main()