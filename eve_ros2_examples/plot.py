#!/usr/bin/env python3

# Set settings up to where it says "No settings after this point"
# Script works on callback of topic

import rclpy
import rclpy.qos
from rclpy.node import Node
import csv
import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np
import os
import time

# Add message class of interest to import, e.g. WholeBodyState
from halodi_msgs.msg import (
    JointName,
    WholeBodyState,
)
# Bebionics use yaml strings
from std_msgs.msg import (
    String
)
import yaml

class WholeBodyStateListener(Node):

    def settings(self):
        # Terminal options
        self.terminal = False

        # Recording options
        self.record = False 
        self.foldername = 'measurements/trial1/plot'
        self.filename = 'plot.csv'
        self.show_record_plot = False # Show plot after done recording

        # Plotting options
        self.plot = True # True currently slows the script down to 30hz, which means data cannot be recorded at original frequency, e.g. 500Hz
        self.history = 100 # how many points to plot in the window, impacts the plot/save rate barely
        self.show_plot_plot = True # Re-open the plot after closing. Not the same as the record_plot because this has limited history

        # The topic and message of interest, add the message_class to imports above
        self.topic =  '/eve/whole_body_state'
        self.message_class = WholeBodyState
        
        # Give the data names, used for plotting and csv file
        # local_time is the first recorded entry, after which come these in order
        # these should align with the read_data output below
        self.data_names = [
            # 'time_eve',
            # '0',
            # '1',
            # '2',
            # '3',
            # '4',
            # '5',
            # '6',
            # '7',
            # '8',
            # '9',
            # '10',
            # '11', 
            # '12',
            # '13',
            # '14',
            # '15',
            # '16',
            # '17',
            # '18',
            # '19',
            # '20',
            # '21', 
            # '22', 

            # 'HIP_YAW',
            # 'HIP_ROLL',
            # 'HIP_PITCH',
            # 'KNEE_PITCH',
            # 'ANKLE_ROLL',
            # 'ANKLE_PITCH',
            'LEFT_SHOULDER_PITCH',
            'LEFT_SHOULDER_ROLL',
            'LEFT_SHOULDER_YAW',
            'LEFT_ELBOW_PITCH',
            'LEFT_ELBOW_YAW',
            'LEFT_WRIST_PITCH',
            'LEFT_WRIST_ROLL',
            # 'RIGHT_SHOULDER_PITCH',
            # 'RIGHT_SHOULDER_ROLL',
            # 'RIGHT_SHOULDER_YAW',
            # 'RIGHT_ELBOW_PITCH',
            # 'RIGHT_ELBOW_YAW',
            # 'RIGHT_WRIST_PITCH',
            # 'RIGHT_WRIST_ROLL',
            ]

    # Function that calls the data of interest from the message
    # May need to do parsing for different types of messages, e.g. bebionic messages are all yaml strings
    # Set self.data_names above to correspond to these entries manually
    def read_data(self, msg):

        # # Example for bebionics:
        # yaml_data = yaml.safe_load(msg)
        # indexFingerCurrent = yaml_data.currentIndexFinger

        # # Example for eve's timestamps:
        # time_eve_s = msg.header.stamp.sec
        # time_eve_ns = msg.header.stamp.nanosec
        # time_eve = time_eve_s + time_eve_ns/10**9

        # data that is saved and plotted with some examples
        data = [
            # indexFingerCurrent,
            # msg.pose.position.x,
            # time_eve,

            # msg.joint_states[0].position,
            # msg.joint_states[1].position,
            # msg.joint_states[2].position,
            # msg.joint_states[3].position,
            # msg.joint_states[4].position,
            # msg.joint_states[5].position,
            # msg.joint_states[6].position,
            # msg.joint_states[7].position,
            # msg.joint_states[8].position,
            # msg.joint_states[9].position,
            # msg.joint_states[10].position,
            # msg.joint_states[11].position,
            # msg.joint_states[12].position,
            # msg.joint_states[13].position,
            # msg.joint_states[14].position,
            # msg.joint_states[15].position,
            # msg.joint_states[16].position,
            # msg.joint_states[17].position,
            # msg.joint_states[18].position,
            # msg.joint_states[19].position,
            # msg.joint_states[20].position,
            # msg.joint_states[21].position,
            # msg.joint_states[22].position,

            # msg.joint_states[JointName.HIP_YAW].position,
            # msg.joint_states[JointName.HIP_ROLL].position,
            # msg.joint_states[JointName.HIP_PITCH].position, 
            # msg.joint_states[JointName.KNEE_PITCH].position,
            # msg.joint_states[JointName.ANKLE_ROLL].position,
            # msg.joint_states[JointName.ANKLE_PITCH].position,

            msg.joint_states[JointName.LEFT_SHOULDER_PITCH].position,
            msg.joint_states[JointName.LEFT_SHOULDER_ROLL].position,
            msg.joint_states[JointName.LEFT_SHOULDER_YAW].position, 
            msg.joint_states[JointName.LEFT_ELBOW_PITCH].position,
            msg.joint_states[JointName.LEFT_ELBOW_YAW].position,
            msg.joint_states[JointName.LEFT_WRIST_PITCH].position,
            msg.joint_states[JointName.LEFT_WRIST_ROLL].position,

            # msg.joint_states[JointName.RIGHT_SHOULDER_PITCH].position,
            # msg.joint_states[JointName.RIGHT_SHOULDER_ROLL].position,
            # msg.joint_states[JointName.RIGHT_SHOULDER_YAW].position, 
            # msg.joint_states[JointName.RIGHT_ELBOW_PITCH].position,
            # msg.joint_states[JointName.RIGHT_ELBOW_YAW].position,
            # msg.joint_states[JointName.RIGHT_WRIST_PITCH].position,
            # msg.joint_states[JointName.RIGHT_WRIST_ROLL].position,
            
            # msg.joint_states[JointName.RIGHT_SHOULDER_YAW].measuredEffort, 
            # msg.joint_states[JointName.RIGHT_SHOULDER_YAW].desiredEffort,
            # msg.joint_states[JointName.RIGHT_ELBOW_PITCH].measuredEffort,
            # msg.joint_states[JointName.RIGHT_ELBOW_PITCH].desiredEffort,
            # msg.joint_states[JointName.LEFT_SHOULDER_YAW].measuredEffort, 
            # msg.joint_states[JointName.LEFT_SHOULDER_YAW].desiredEffort,
            # msg.joint_states[JointName.LEFT_ELBOW_PITCH].measuredEffort,
            # msg.joint_states[JointName.LEFT_ELBOW_PITCH].desiredEffort,
            ]
        return data

#--------------------------------------------------------------------------------------------------------------------------
# No settings after this point
#--------------------------------------------------------------------------------------------------------------------------
    
    def __init__(self, whole_body_command_msg=None):
        super().__init__(
            "measurement"
        )

        # Contains all user settings above
        self.settings()

        # Terminal init
        if self.terminal:
            self.init_terminal()

        # Plotting init
        if self.plot:
            self.init_plot()

        # Recording init
        if self.record:
            self.init_record()

        # subscribe to chosen topic and attach callback
        self._subscriber = self.create_subscription(
            self.message_class, self.topic, self.topic_callback, rclpy.qos.qos_profile_system_default
        )

    def init_terminal(self):
        pass

    def init_record(self):
        print('Start recording')

        # where to save the file, can add more argument to join more together
        self.folderpath = os.path.join(os.getcwd(), self.foldername)
        self.filepath = os.path.join(self.folderpath, self.filename)

        # create directory if not exists
        if not os.path.exists(self.folderpath):
            os.makedirs(self.folderpath)

        # open a csv writer with columns names
        self.file = open(self.filepath, 'w', newline='')
        self.writer = csv.writer(self.file)
        # initialize default (time) column names and the custom data
        row = ['time_local'] + self.data_names
        self.writer.writerow(row)

    def init_plot(self):
        print('Start plotting')

        # initialize buffers
        self.t = [0]*self.history
        # create zero buffer for each of the variables, but slice to not reference to the same buffer
        self.n = len(self.data_names)
        self.y = [self.t[:] for i in range(self.n)]
        # need a "first time" bool for plotting
        self.first = True

        # open plot
        plt.ion()
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        # plot each initial plot, and save line classes in self.lines
        self.lines = []
        for Y in self.y:
            self.lines.append(self.ax.plot(self.t, Y))
        
        # initialize text 
        self.text = self.ax.text(0,0, "")

        # visual
        set_style(self.fig, self.ax)
        
        # setup plot
        self.axbackground = self.fig.canvas.copy_from_bbox(self.ax.bbox)
        self.fig.canvas.draw()   # note that the first draw comes before setting data 
        plt.show(block=False)
        
        # connect a callback when closing the figure
        self.fig.canvas.mpl_connect('close_event', self.on_close)

    # ran every time topic is received
    def topic_callback(self, msg):
        
        # read time stamped in this python node
        time_local_s, time_local_ns = self.get_clock().now().seconds_nanoseconds()
        self.time_local = time_local_s + time_local_ns/10**9

        # read the topic message data and convert to strings
        self.data_list = self.read_data(msg)
        self.data_list_string = ["\n" + str(X) for X in self.data_list]

        if self.terminal:
            self.terminal_function()
            
        if self.record:
            self.record_function()

        if self.plot:
            self.plot_function()

    def terminal_function(self):
        # output to logger (terminal output)
        self.get_logger().info(str(self.time_eve) + ' ' + str(self.time_local) + "".join(self.data_list_string))

    def plot_function(self):
        # move the time buffers along
        self.t[:-1] = self.t[1:]
        self.t[-1] = self.time_local

        # or, if the first time, itialize to all the same values
        if self.first:
            self.t = [self.time_local]*self.history
        
        # differences can be useful to have
        self.dt = np.diff(self.t)
        self.dt_average = np.average(self.dt)

        # set the framerate string
        # but only after buffer has been filled (not the same values in self.dt)
        if (len(set(self.dt)) is not len(self.dt)):
            self.dt_average = np.NAN
        plt.title('Live data \n Mean frame and log rate:\n {fps:.3f} Hz'.format(fps= 1/self.dt_average))


        # for each line, theres probably a pythonic way to do this
        for i in range(0, self.n):
            # move the data buffer along
            self.y[i][:-1] = self.y[i][1:]
            self.y[i][-1] = self.data_list[i]
            # or, if the first time, itialize to all the same values
            if self.first:
                self.y[i] = [self.data_list[i]]*self.history
            # set each line's data
            self.lines[i][0].set_xdata(self.t)
            self.lines[i][0].set_ydata(self.y[i])

        # disable the initial code
        self.first = False
        
        # limits are max of whatever in the current buffers
        # fix for the first times dt is all the same, add small number
        self.ax.set_xlim(np.min(self.t), np.max(self.t) + 1e-3)
        # or, min and max plus 10 percent of the difference
        diff = np.max(self.y) - np.min(self.y)
        self.ax.set_ylim(np.min(self.y) - diff/10, np.max(self.y) + diff/10)

        # why does the legend keep moving around?
        plt.gca().legend(self.data_names)
        # plt.legend(loc="upper left")

        # restore background
        self.fig.canvas.restore_region(self.axbackground)

        # redraw just the lines and text
        for l in self.lines:
            self.ax.draw_artist(l[0])
        self.ax.draw_artist(self.text)

        # fill in the axes rectangle
        self.fig.canvas.blit(self.ax.bbox)

        # flush
        self.fig.canvas.flush_events()

    def record_function(self):
        # write values to file
        # local time first, then the data list
        row = [self.time_local] + self.data_list
        self.writer.writerow(row)

    def on_close(self, event):

        # when figure is closed, stop listening
        self._subscriber.destroy()

    def exit_terminal(self):
        pass

    def exit_plot(self):
        print('Stop plotting')

        if self.show_plot_plot:
            # replot
            fig_post_plot = plt.figure()
            ax_post_plot = fig_post_plot.add_subplot(111)
            for Y in self.y:
                ax_post_plot.plot(self.t, Y)

            # visual
            set_style(fig_post_plot, ax_post_plot)
            plt.gca().legend(self.data_names)
            plt.title('Data in the plotting buffer (last {his} values) \n mean frame rate:\n {fps:.3f} Hz'.format(his=self.history, fps= 1/self.dt_average))

            # show
            plt.show(block=True)

    def exit_record(self):
        print('Stop recording')
        # Close file after stopping
        self.file.close()

        if self.show_record_plot:
            # Read file
            df = pd.read_csv(self.filepath)
            
            # difference over all recorded data times
            average_all = np.average(np.diff(df['time_local']))

            # Plot what has been saved to file with pandas
            fig_post_record = plt.figure()
            ax_post_record = df.plot(x='time_local')
            plt.title('Recorded data \n mean log rate:\n {fps:.3f} Hz'.format(fps= 1/average_all))

            # visual
            set_style(fig_post_record, ax_post_record)

            plt.show(block=True)

def set_style(fig, ax):
    # for consistency between plots
    color = [1,1,1]
    ax.set_facecolor(color)
    fig.patch.set_facecolor(color)
    plt.grid(which='major')

    # Minor ticks slow down plotting
    # plt.minorticks_on()
    # plt.grid(which='minor', color='grey', linestyle = '--')

def main():

    rclpy.init()  

    node = WholeBodyStateListener()

    # Spin here keeps the script waiting for what its subscribed to instead of finishing
    try:
        rclpy.spin(node)
    # until ctrl+c is pressed, then exit the interrupts and finish the main()
    except KeyboardInterrupt:
        pass

    if node.terminal:
        node.exit_terminal()

    if node.plot:
        node.exit_plot()

    if node.record:
        node.exit_record()

    rclpy.shutdown()

if __name__ == "__main__":
    main()
