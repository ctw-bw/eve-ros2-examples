#!/usr/bin/env python3

import uuid

import numpy as np
import rclpy
import rclpy.qos
from action_msgs.msg import GoalStatus
from builtin_interfaces.msg import Duration
from std_msgs.msg import String
from halodi_msgs.msg import (
    JointName,
    JointSpaceCommand,
    ReferenceFrameName,
    TaskSpaceCommand,
    TrajectoryInterpolation,
    WholeBodyTrajectory,
    WholeBodyTrajectoryPoint,
    DrivingCommand,
)
from rclpy.node import Node
from scipy.spatial.transform import Rotation
from unique_identifier_msgs.msg import UUID

def create_message():

    msg = DrivingCommand()
    msg.linear_velocity = 0.5

    return msg

class DemoDriveThroughNode(Node):
    def __init__(self) :
        super().__init__("demo_drive_through")

        self.publisher_driving_command = self.create_publisher(
            DrivingCommand, "/eve/driving_command", rclpy.qos.qos_profile_action_status_default 
        )

        # demo status sequencing
        self.publisher_demo_state = self.create_publisher(
            String, "/eve/demo_state",  rclpy.qos.qos_profile_action_status_default 
        )

        self.subscriber_demo_state = self.create_subscription(
            String, "/eve/demo_state", self.demo_state_cb, 10
        )

        self.t = 0.0
        self.dt = 0.1
        self.tmax = 2

    def demo_state_cb(self, msg):
        # if previous state is done, drive forward for 3 sec
        print('demo_state_cb')

        if msg.data == '5':
            print('publishing wbt')
            self.timer = self.create_timer(self.dt, self.timer_cb)
        
    def timer_cb(self):
        print('timer_cb', self.t)
        # drive for 3 sec, if done driving publish new state and stop
        self.t += self.dt

        msg_out = create_message()
        self.publisher_driving_command.publish(msg_out)

        if self.t >= self.tmax:
            msg = String()
            msg.data = '6'

            self.publisher_demo_state.publish(msg)

            self.destroy_node()

def main():

    rclpy.init()  

    node = DemoDriveThroughNode()

    # Spin here keeps the script waiting for what its subscribed to instead of finishing
    try:
        rclpy.spin(node)
    # until ctrl+c is pressed, then exit the interrupts and finish the main()
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()

if __name__ == "__main__":
    main()