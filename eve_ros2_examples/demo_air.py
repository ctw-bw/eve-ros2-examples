#!/usr/bin/env python3

# Copyright 2021 Halodi Robotics
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import uuid

import numpy as np
import rclpy
import rclpy.qos
from action_msgs.msg import GoalStatus
from builtin_interfaces.msg import Duration
from std_msgs.msg import String
from halodi_msgs.msg import (
    JointName,
    JointSpaceCommand,
    ReferenceFrameName,
    TaskSpaceCommand,
    TrajectoryInterpolation,
    WholeBodyTrajectory,
    WholeBodyTrajectoryPoint,
)
from rclpy.node import Node
from scipy.spatial.transform import Rotation
from unique_identifier_msgs.msg import UUID

def generate_task_space_command_msg(
    body_frame_id, expressed_in_frame_id, xyzrpy, z_up=True
):
    """Generates a task space command msg.

    Parameters:
    - body_frame_id (enum): body part to be moved, e.g. ReferenceFrameName.PELVIS
    - expressed_in_frame_id (enum): reference frame for body_frame_id, e.g. ReferenceFrameName.BASE
    - xyzrpy (array of 6 floats): desired pose of body_frame_id relative to expressed_in_frame_id
      , as a list/tuple/1D np.array of [ posX, posY, posZ, rotX, rotY, rotZ ]
    - z_up (bool): whether or not xyzrpy follows the Z-up co-ordinate convention. Default: True

    Returns: TaskSpaceCommand msg
    """

    msg_ = TaskSpaceCommand(express_in_z_up=z_up)
    msg_.body_frame.frame_id = body_frame_id
    msg_.expressed_in_frame.frame_id = expressed_in_frame_id

    msg_.pose.position.x = xyzrpy[0]
    msg_.pose.position.y = xyzrpy[1]
    msg_.pose.position.z = xyzrpy[2]
    quat_ = Rotation.from_euler("xyz", xyzrpy[3:]).as_quat()  # Euler to quaternion
    msg_.pose.orientation.x = quat_[0]
    msg_.pose.orientation.y = quat_[1]
    msg_.pose.orientation.z = quat_[2]
    msg_.pose.orientation.w = quat_[3]

    return msg_

def create_message():
    NOMINAL_PELVIS_HEIGHT_ABOVE_BASE = 0.91
    cumulative_seconds_from_start_ = 0

    cumulative_seconds_from_start_ = cumulative_seconds_from_start_ + 5
    periodic_trajectory_pt_msg_1_ = WholeBodyTrajectoryPoint(
        time_from_start=Duration(sec=cumulative_seconds_from_start_)
    )  # create a trajectory point msg, timestamped for 3 seconds in the future
    periodic_trajectory_pt_msg_1_.task_space_commands.append(
        generate_task_space_command_msg(
            ReferenceFrameName.RIGHT_HAND,
            ReferenceFrameName.PELVIS,
            [0.25, -0.55, 0.9, np.deg2rad(00.0), -np.deg2rad(180.0), -np.deg2rad(90.0)],
        )
    )  # append a desired task space pose for the pelvis WRT base
    # [posX, posY, posZ, roll, pitch, yaw]
    periodic_trajectory_pt_msg_1_.task_space_commands.append(
        generate_task_space_command_msg(
            ReferenceFrameName.LEFT_HAND,
            ReferenceFrameName.PELVIS,
            [0.25, 0.55, 0.9, 0.0, -np.deg2rad(180.0), np.deg2rad(90.0)],
        )
    )  # append a desired task space pose for the pelvis WRT base
    # [posX, posY, posZ, roll, pitch, yaw]


    periodic_trajectory_msg_ = WholeBodyTrajectory(
        append_trajectory=False
    )  # create a whole body trajectory msg that will
    # override any trajectory currently being executed
    periodic_trajectory_msg_.interpolation_mode.value = (
        TrajectoryInterpolation.MINIMUM_JERK_CONSTRAINED
    )  # choose an interpolation mode
    periodic_trajectory_msg_.trajectory_points.append(periodic_trajectory_pt_msg_1_)

    return periodic_trajectory_msg_

class WholeBodyTrajectoryPublisher(Node):
    def __init__(self, initial_trajectory_msg=None, periodic_trajectory_msg=None):
        super().__init__(
            "demo_air"
        )  # initialize the underlying Node with the name demo_air

        # 10 is overloaded for being 10 deep history QoS
        self._publisher_whole_body_trajectory = self.create_publisher(
            WholeBodyTrajectory, "/eve/whole_body_trajectory", rclpy.qos.qos_profile_action_status_default 
        )

        self._subscriber_whole_body_trajectory_status = self.create_subscription(
            GoalStatus, "/eve/whole_body_trajectory_status", self.goal_status_cb, 10
        )  # create a GoalStatus subscriber with inbound queue size of 10


        # demo status sequencing
        self._publisher_demo_state = self.create_publisher(
            String, "/eve/demo_state",  rclpy.qos.qos_profile_action_status_default 
        )

        self._subscriber_demo_state = self.create_subscription(
            String, "/eve/demo_state", self.demo_state_cb, 10
        )

        self._periodic_trajectory_msg = periodic_trajectory_msg

    def demo_state_cb(self, msg):
        print('in demo callback')

        periodic_trajectory_msg_ = create_message()

        if msg.data == '4':
            print('publishing wbt')
            self._publisher_whole_body_trajectory.publish(periodic_trajectory_msg_)

    def goal_status_cb(self, msg):

        if msg.status == GoalStatus.STATUS_SUCCEEDED:
            self.get_logger().info("Goal succeeded, publish state update")

            msg = String()
            msg.data = '5'
            self._publisher_demo_state.publish(msg)
            self.destroy_node()

def main():

    rclpy.init()  

    node = WholeBodyTrajectoryPublisher()

    # Spin here keeps the script waiting for what its subscribed to instead of finishing
    try:
        rclpy.spin(node)
    # until ctrl+c is pressed, then exit the interrupts and finish the main()
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()

if __name__ == "__main__":
    main()